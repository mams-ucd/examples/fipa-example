import cartago.Artifact;
import cartago.OPERATION;

public class TestArtifact extends Artifact {
    @OPERATION
    public void pause() {
        try {
            System.out.println("Sleeping");
            Thread.sleep(10000);
            System.out.println("Slept");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}